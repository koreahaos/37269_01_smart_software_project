# 37269-01 Smart Software Project

* **For The Publics**
    * Haos' Take Away Public [Page](http://koreahaos.github.io/37269_01_smart_software_project/)

# BUILDING!!

#### Useful links:

* **Ewha Womans**
    * Smart Software Project [Home](http://cyber.ewha.ac.kr/course/view.php?id=36064)
    * Smart Software Project [Files](http://cyber.ewha.ac.kr/mod/ubboard/view.php?id=278881)
    * Smart Software Project [Assignments](http://cyber.ewha.ac.kr/mod/assign/index.php?id=36064)

* **Arduino IDE**
    * [CodeBender](https://codebender.cc/home) Online, in the cloud...
    * [Instructor Supplied Androx Studio IDE](https://www.dropbox.com/s/1jooitdit1dg8g3/Androx.zip?dl=0)

* **Eclipse and Arduino**
    * [Eclipse/Arduino IDE](http://eclipse.baeyens.it/index.shtml)
    * 

* **Hanback**
    * SmartCAR manufacturer [page](http://www.hanback.co.kr/insiter.php?design_file=1143.php&category_1=C&search_value=&PB_1446318362=1&article_num=51)

* **Code Cleanup:**
    * [prettyprinter](http://prettyprinter.de/index.php)


* **ToDo:**
    * Create lab_write_ups
    * Create gh-pages page.
        * link to page : http://koreahaos.github.io/37269_01_smart_software_project/
        * link to public repo : https://github.com/KoreaHaos/37269_01_smart_software_project
        * Make public gh-pages branch default and delete master branch.
    * Figure out way to push public seperate from private.


* **DONE:**
    * merge VM 'smart_software_project_cleanup'
    * merge VM 'smart_software_project_burner'

BLUEOOTH PAIRING CODE BTWIN



