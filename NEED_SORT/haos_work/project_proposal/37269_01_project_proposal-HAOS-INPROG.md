## Smart Software Project 37269-01

#### Project Proposal

[Ewha W. University](http://www.ewha.ac.kr/) 

## Haos Find !!!!! FIX THIS !!!!

### 1. Project Statement

* This project, produced in the second quarter of the spring 2016 semester will utilize many facets of Computer Science study. Bringing together Google's Android Application Ecosystem, Arduino Embedded Systems, Web Technologies, and World-Wide Research; Ewha Sspplied tools and knowledge will be couple to Haos inginuity in a bold attempt at doing what has not been done before. 

### 3.  Project description

* A self-propelled ground based device will motivate itself to a pre-defined location relative to a location unknown object and/or person.

* Who
* Professor HyungJune Lee along with TA <!!i_need_a_nameHere!!> will guide Team Haos.
* What
* An [Arduino Mega 2560](https://www.arduino.cc/en/Main/arduinoBoardMega2560) coupled with and Android Nexus 7 to controlled a multi-sensored SmartCar platform. 
* Where
* Ewha Womans Univeristy - Seoul - South Korea!
* When
* Spring 2016
* Why
* Because it is Awesome!
* How
* Hard Work and Strong Dedication.

### 4.  Contribution of Work.

* The proccess, procedures, and intelectul product of this work will be made publicly available for consumption by the open-source community. 

### 5.  Related work

  * There are many attempts and various methods have been explored.
  * Tracking a ball.
  * https://www.youtube.com/watch?v=O6j02lN5gDw
  * Face detection and tracking.
  * http://www.instructables.com/id/Face-detection-and-tracking-with-Arduino-and-OpenC/
  * **Key Difference**
  * Neither projects moves the device/camera in all three dimensions.

### 6. System overview & Architecture


	
### 7.  .Verification procedure

  * **How can you test if your project works properly as designed**
  * If the device can locate a common ball, hung from a string.
  * If the device can determine the height of the ball above the ground.
  * If the device can motivate itself to a preset distance from the ball's plumb line.
  * If the device can position itself at a specified face of the ball.
  * **Test cases**
  * Get ball.
  * Hang ball from string somewhere in the area of the device.
  * Turn on device.
  * See what happens.
  * (See above for general break-down of each stage.)

### 9. What do you anticipate will be the easiest part of your project?

  * Moving the device.
  * Finding examples and tutorial that can be applied to the project.
  * Creating documentation of the project.
  * 
### 10.  What do you anticipate will be the most difficult part of your project? :

  * Coupling the Arduino, Android, and World-Wide-Web.
  * Anologizing the object recognition.
  * Utilization of tools and resources in a langauge not understood.
  * Multiple intensive projects in several classes, both in Korea and in US, are creating large demands for time and effort.

### 11.Detatiled time plan

* **Detailed time plan**

#### **Week 6**	
***2016/04/11(MON)*
* (Due: Project proposal)
* Wait for comments on proposal.

#### **Week 7**	
*2016/04/18(MON)*
* Exam Period.
#### **Week 8**	
*2016/04/25(MON)*
* Determine if object avoidence will be feasable.
#### **Week 9**	
*2016/05/02(MON)*
* General research.
#### **Week 10**	
*2016/05/09(MON)*
* General research.
#### **Week 11**	
*2016/05/16(MON)*
* Connection alrythmically of camera and motors.
#### **Week 12**	
*2016/05/23(MON)*
* Team meeting (for progress check)
#### **Week 13**	
*2016/05/30(MON)*
* Team meeting (for progress check)
#### **Week 14**	
*2016/06/06(MON)*
* Final push for completion.
*(Memorial Day)*
#### **Week 15**	
#### *2016/06/13(MON)*
* Project presentation & demo 
*(Due: source code, presentation slides, & poster slide)*

### 12. Refernce

