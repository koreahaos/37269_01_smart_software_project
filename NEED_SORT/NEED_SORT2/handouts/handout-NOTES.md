# Notes for : 37269-01 Lecture Week 5
# UART & Timer

Professor HyungJune Lee *(hyungjune.lee@ewha.ac.kr)*

### Contents

* Review of last lecture:
    * ATmega 2560 I/O Register
* **Midterm : 2pm - 4:30pm, Mon Apr 11**
* ATmega2560 microcontroller (MCU):
    * UART
    * Timer
* Announcement


## Term Project!

### Project Proposal Submission

* **Project Proposal** *(One per team)*
    * Due: **Friday April 22 @ 5pm**
    * What to submit:
        * Report hardcopy *(printed report)*
        * Report softcopy *(Submit report file to Cyber Campus)*
    * Where to submit:
        * HW box at "HyungJune Lee" in front of Asan 221-1
    * Language: English or Korean
    * Start discussing what your team will do with your team partner.


Project Proposal Templet
* Project name (in English)
* Project statement
    * Goals of your project
* Project description
    * What will your project be doing?
    * Key functions
* Contributions of your work to research or industry
* Related work
    * Any existing previous works (at least two) similar to your project
    * What’s similar and different?
* System overview & architecture
    * Block diagram of main blocks
    * What each block is doing
    * How each block is connected to other blocks, i.e., interface
        * Any message is exchanging between blocks? e.g., request/reply, data, etc? 
* Development environment
    * Arduino Mega 2560-based SmartCAR (or others if any, e.g., Android device)
    * Androx Studio IDE (or others if any)
    * What kind of sensors are to be used in your project
    * Other information from the connection to Android device or otherdevices? (location, Internet, etc.)
* Verification procedure
    * How can you test if your project works properly as designed
    * Test cases
* What do you anticipate will be the easiest part of your project?
* What do you anticipate will be the most difficult part of your project?
* Detailed time plan
* References

Evaluation criteria
* Format requirement
    * 5 points
* Creativity
    * 5 points
* Clarity
    * 5 points
* Concreteness (of software architecture)
    * 5 points
* Implementability
    * 5 points

* Total score: 25 points


| Week                                                                                            | Lecture Contents | Lab Contents |
|-------------------------------------------------------------------------------------------------|------------------|--------------|
| Week 1 | Course introduction  |Arduino introduction: platform &amp; programming environment |
| Week 2 | Embedded system overview &amp; source management in  collaborative repository (using GitHub) | Lab 1: Arduino Mega 2560 board &amp; SmartCAR platform |
| Week 3 | ATmega2560 Micro-controller (MCU): architecture &amp; I/O ports, Analog vs. Digital, Pulse Width Modulation | Lab 2: SmartCAR LED control |
| Week 4 | Analog vs. Digital &amp; Pulse Width Modulation | Lab 3: SmartCAR motor control (Due: HW on creating project repository using GitHub) |
| Week 5 | ATmega2560 MCU: memory, I/O ports, UART | Lab 4: SmartCAR control via Android Bluetooth  |
| Week 6 | ATmega2560 UART control &amp; Bluetooth communication between Arduino platform and Android device | Lab 5: SmartCAR control through your own customized Android app (Due: Project proposal) |
| Week 7 | Midterm exam  |              |
| Week 8 | ATmega2560 Timer, Interrupts &amp; Ultrasonic sensors | Lab 6: SmartCAR ultrasonic sensing |
| Week 9 | Infrared sensors &amp; Buzzer | Lab 7: SmartCAR infrared sensing  |
| Week 10 | Acquiring location information from Android device &amp; line tracing | Lab 8: Implementation of line tracer    |
| Week 11 | Gyroscope, accelerometer, and compass sensors | Lab 9: Using gyroscope, accelerometer, and compass sensors  |
| Week 12 | Project Team meeting | (for progress check)   |
| Week 13 | Project Team meeting | (for progress check)   |
| Week 14 | Course wrap-up &amp; next steps  |              |
| Week 15 | Project presentation &amp; demo (Due: source code, presentation slides, &amp; poster slide) | Project presentation &amp; demo II |
| Week 16 | Final week (no final exam) |              |


* Review from the last lecture
    * ATmega 2560 I/O Register
* Midterm: **2pm - 4:30pm, Mon Apr 11**
* ATmega2560 microcontroller (MCU)
    * UART
    * Timer
* Announcement



### Data Transmission Tree

```
Data Transmission
|--  Parallel
|-+-  Serial
  |--  Synchronous
  |--  Asynchronous
```

#### Definition: Parallel

* Data is sent and received more than one bit at a time.
* Transmission on multiple wires (8bits => 8 wires).
* Many lines of communication, synchronized bursts of data.

#### Definition: Serial
* Data is sent and received one bit at a time.
* Transmission on single wire.
* One line of communication, long string of data.

#### Serial Types: RS232, SCI, and SPI

* **RS232**
    * Typical computer COM port.
    * RS-232 was first introduced in 1962 by the Radio Sector of the EIA.

* **SCI**
    * **S**erial **C**ommunication **I**nterface : uses the universal asynchronous receiver/transmitter know as : **UART**.
    * Typically from one device to another.

* **SPI**
    * **S**erial **P**eripheral **I**nterface : part of Port B
    * Used for comm between chips in computer.

#### Why Serial?
* Fewer wires translates to:
    * Lower cost.
    * Simpler set-up.

### Types of Serial Data Transmission.

#### Definition: Synchronous
* Sender and Receiver have their clocks synchronized.
* Transmissions occur at specified intervals.
* Advantage:
    * Faster.
    * No control data needed.



#### Definition: Asynchronous
* Devices are not synchronized.
* Sender and Receiver can have their clocks un-synchronized.
* Transmissions happen at unpredicted intervals.
* Advantages:
    * Simpler.
    * More robust.

#### Why Asynchronous?
* Disadvantage:
    * Slower due to overhead.
* Advantages:
    * Simpler.
    * Cheaper.
    * Information can be sent when ready.

#### Please Note:
* Both synchronous and asynchronous **must have agreed upon bit transfer rate.**

#### What is : "UART"
* **U**niversal
* **A**synchronous
* **R**eceiver-
* **T**ransmitter

*"...a computer component that handles asynchronous serial communication."* - www.webopedia.communication

#### UART Character Frame & Character Frame Example

*see slides 19 & 20*

* First bit (0), start bit, signals transmission from sender.
* Second through eighth bits (???????), the data.
* Ninth bit (?), parity or check bit, last data bit used to indicating if number of bits transmitted is even or odd.
* Last two bits (11), end bits, signal end of transmission.

Example :


00001101111
0123456789a

0 - Start Bit

1 - data bit 0
... (data = 0001101)
7  - data bit 6

8 - parity bit 

9 - stop bit 1
a - stop bit 2

#### Definitions

* **Start Bit:**
    * Signals the beginning of the data word.
    * A low bit after a series of high bits.
* **Data Bits:**
    * The meat of the transmission.
    * Usually 7 or 8 bits.
* **Parity Bit:**
    * An error check bit placed after the data bits.
    * Can be high or low depending on whether odd parity or even parity is specified.
* **Stop Bit/s:**
    * One or two high bits that signal the end or the data word.
* **Data Word:**
    * Start Bit, Data Bits, Parity Bit, & Stop Bit/s.
    *  *Refers to all the above bit as a whole.*

#### BAUD RATE & BIT RATE

* Baud Rate = bits transferred/second.
* Baud Rate INCLUDES start, stop, and parity.
* "bit rate" refers to JUST data bits transferred per second (may include parity).
* baud rate > bit rate


#### ATmega2560 USART Port

* Send: 8-bit parallel data are converted to serial data, and sent.
* Receive: Serial data are converted to parallel data that is then sent to CPU.
* The ATmega2560 has four USART (**U**niversal **S**ynchronous/**A**synchronous **R**eceiver/**T**ransmitter) ports.
    * USART0, USART1, USART2, USART3
    * Full-Duplex
        * Send/Recieve at same time.
    * Support both Synchronous mode and Asynchronous modes.
    * Asynchronous transmission mode uses:
        * Interrupts
            * TX Complete
            * TX Data Register Empty
            * RX Complete

* ATmega2560 USART Data Frame Format
    * USART data frame
        * Start bit:
            * 1bit ‘0’ is automatically generated upon transmission.
        * Data bit:
            * 5, 6, 7, 8, 9 bits.
        * Parity bit:
            * 0 or 1 parity bit (odd parity or even parity).
        * Stop bit:
            * 1 or 2 stop bits ‘1’ are automatically generated upon transmission.

#### **Arduino Serial Port API**

##### Standard/Built-In Functions:
* `Serial.begin(speed)`
    * Parameter `speed` sets baud rate for the USART port.
        * Potential values or TX/RX speeds:
            * 2400, 4800, 9600, 14400, 19200, 28800, 38400, 57600, 115200
    * To use USART, you should use this function to initialize the serial port.
    * For each USART:
        * For USART0 : `Serial.begin(speed)`
        * For USART1 : `Serial1.begin(speed)`
        * For USART2 : `Serial2.begin(speed)`
        * For USART3 : `Serial3.begin(speed)`
* `Serial.end()`
    * Disable the USART.
* `Serial.available()`
    * Get the number of bytes (characters) available for reading from the serial port.
    * The data has already arrived and stored in the serial receive buffer.
* `Serial.read()`
    * Read incoming 1 byte data from the serial port.
    * After reading, the data is erased in the serial receive buffer.
* `Serial.peek()`
    * Read incoming 1 byte data from the serial port
    * After reading, the data is not erased in the serial receive buffer
        * After reading data using peek(), if you use read(), the returned data are the same as before
* `Serial.write(val)`
    * Write binary data and send them to the serial port.
    * Parameter `val` = a single byte or a string consisting of a series of bytes.
    * Function returns the number of bytes written and sent.

    * Reference: http://arduino.cc/en/Reference/Serial

* `Serial.print(val, format)`
    * Print data to USART as human-readable ASCII text.
        * Parameter `val`: string or data to send via serial
        * Parameter `format`: number base (for integer type) or number of decimal places (for floating point type)
    * Function returns the number of bytes written.
    * Serial.print(val, format) Example
        * `Serial.print(78)` => "78" will be sent.
        * `Serial.print('N')` => "N" will be sent.
        * `Serial.print("Hello world.");` => "Hello world." will be sent.
        * `Serial.print(78, BIN)` => "1001110" will be sent in ASCII
        * `Serial.print(78, OCT)` => "116" will be sent in ASCII
        * `Serial.print(78, DEC)` => "78" will be sent in ASCII
        * `Serial.print(78, HEX)` => "4E" will be sent in ASCII
        * `Serial.print(1.23456)` => "1.23" will be sent (rounded to the 2nd decimal place by default)
        * `Serial.print(1.23456, 0)` => "1" will be sent (rounded to the 0th decimal place)
        * `Serial.print(1.23456, 2)` => "1.23" will be sent (rounded to the 2nd decimal place by default)
        * `Serial.print(1.23456, 4)` => "1.2346" will be sent (rounded to the 4th decimal place by default)
* `Serial.println(val, format)`
    * Print data to the serial port as human-readable ASCII text followed by a carriage return character (‘\r’) and a newline character(‘\n’)
* `Serial.flush()`
    * Wait for the transmission of outgoing serial data to complete.
    * If `Serial.write()` is called, data will be transmitting in background.
    * Sometimes, you need to wait for TX to be completed.
        * `Serial.write()`
        * `Serial.flush()` // Causes program to wait for the TX to be completed.
* `void serialEvent()`
    * Is triggered upon receiving data from USART:
        * On USART0, `serialEvent()` is called.
        * On USART1, `serialEvent1()` is called.
        * On USART2, `serialEvent2()` is called.
        * On USART3, `serialEvent3()` is called.
    * Inside `void serialEvent()` function, `Serial.read()` should be used to read data.



**SmartCAR UART Port Configuration**

* UART0 port is used for both program port to PC and Bluetooth port to Bluetooth wireless.
    * Cannot be used at the same time!
* UART0 can be useful for debugging your Arduino programs.
* Baud rate should be set to 115,200 bps 

| UART No.  | Name   | Port / Number   | Etc  |
|-----------|--------|-----------------|------|
| UART0   | RXD0 / TXD0 | PE0 / PE1  | Program port /  Bluetooth port  |
| UART1  | RXD1 / TXD1  | PD2 (19) / PD3 (18)  | Ultrasonic sensor  |
| UART2  | RXD2 / TXD2 | PH0 (17) / PH1 (16)  | Extension board 1  |
| UART3 |RXD3 / TXD3 | PJ0 (15) / PJ1 (14)  | Extension board 2  |

### SmartCAR UART Example

***File** : `UART_Echo.cpp`*

```cpp
#include "UART_Echo.h"

// Variable for initial comm. message.
unsigned char text[] = "\r\n Welcome! Arduino Mega 2560 \r\n UART0 Test Program.\r\n";

void setup()
{
    Serial.begin(115200);   // Set the baud rate for serial port.
    
    // Display welcome message.
    
    int index=0;
    
    while(text[index] !='\0')   // Loop through text array until all letters sent.
    {
        Serial.write(text[index++]);
    }
    
    // Display a prompt for programs output.
    Serial.print("ECHO >> ");
}

void loop()
{
    // Synopsis : If serial recieves input, write it back to output.
    
    // If Serial.available() is greater than zero, the buffer contains data.
    
    if(Serial.available() > 0) {
        
        // write the data read from serial.
        Serial.write(Serial.read());
    }
}
```

# Arduino Timer : Clock & Counter

* Clock
    * Fundamental "Clock" in computer system.
    * Generated by an oscillator crystal.
    * Period: one cycle of a '1' and a '0' signal.
    * Counter: counts the number of clock cycles.

* Arduino Timers:
    * `Timer0` & `Timer2`
        * 8 bit counter. (counts : 0 - 255)
    * `Timer1` `Timer3`, `Timer4` & `Timer5`
        * 16 bit counter. (counts :  0 - 65535)
    
    * Timerx function (x =0~5)
        * Timerx library function: use timer and counter x
        * Timer0 & Timer2
            * 8-bit timer: count 0 ~ 255, and then when it comes to 0, it will generate a timer overflow interrupt
        * All other timers, Timer1, Timer3, Timer4, & Timer5
            * 16-bit timer: count 0 ~ 65535, and then when it comes to 0, it will generate a timer overflow interrupt

* 3 Functions
    * Timerx::set(unsigned long us, void (*f)())
        * Set an timer period & Interrupt Service Routine function
            * us: interrupt period in micro second
            * void(*f)(): Interrupt Service Route to execute when an timer overflow interrupt occurs
    * Timerx::start()
        * Start the timer
    * Timerx::stop()
        * Stop the timer

# ATmega2560 Timers

```cpp

#include <Timer2.h>
#define FRONT_LED 10

int LED_state = 0;

void Timer2_ISR() {
    digitalWrite(FRONT_LED, LED_state);

    if (LED_state)
        LED_state = 0;
    else
        LED_state = 1;
}

void setup() {
    pinMode(FRONT_LED, OUTPUT);
    Timer2::set(1000000, Timer2_ISR);
    Timer2::start();
}

void loop() {
}
```


# Delay vs. Timer Approach


### delay Example

```cpp
#define FRONT_LED 10
int LED_state = 0;
void setup() {
    pinMode(FRONT_LED, OUTPUT);
}
void loop() {
    digitalWrite(FRONT_LED, LED_state);
    delay(1000);
    if (LED_state)
    LED_state = 0;
    else
    LED_state = 1;
}
```

### Timer Example

```cpp
#include <Timer2.h>
#define FRONT_LED 10
int LED_state = 0;
void Timer2_ISR() {
    digitalWrite(FRONT_LED, LED_state);
    if (LED_state)
    LED_state = 0;
    else
    LED_state = 1;
}
void setup() {
    pinMode(FRONT_LED, OUTPUT);
    Timer2::set(1000000, Timer2_ISR);
    Timer2::start();
}
void loop() {
}
```

# Notes for : 37269-01 Lab Week 5


### Week 4 Lab Assignment #3 Solution Example :

Program should :

1. Move forward for 2 seconds
2. Stop for 0.5s
3. Turn right for 1.5s
4. Stop for 0.5s
5. Turn left for 1.5s
6. Stop for 0.5s
7. Move backward for 2 seconds
8. Stop forever!

*NOTE : Make sure to have a break for at least 500ms between different operations*

```cpp
#define LEFT_MD_A 22
#define LEFT_MD_B 23
#define RIGHT_MD_A 24
#define RIGHT_MD_B 25
#define LEFT_MOTOR_EN 4
#define RIGHT_MOTOR_EN 5

int init_done = false;

//The setup function is called once at startup of the sketch

void setup()
{
    pinMode(LEFT_MD_A, OUTPUT);
    pinMode(LEFT_MD_B, OUTPUT);
    pinMode(RIGHT_MD_A, OUTPUT);
    pinMode(RIGHT_MD_B, OUTPUT);
    pinMode(LEFT_MOTOR_EN, OUTPUT);
    pinMode(RIGHT_MOTOR_EN, OUTPUT);

    digitalWrite(LEFT_MD_A, LOW);
    digitalWrite(LEFT_MD_B, LOW);
    digitalWrite(RIGHT_MD_A, LOW);
    digitalWrite(RIGHT_MD_B, LOW);
    digitalWrite(LEFT_MOTOR_EN, LOW);
    digitalWrite(RIGHT_MOTOR_EN, LOW);
}

void move_forward()
{
    //Rotate counterclockwise for left motor
    digitalWrite(LEFT_MD_A, HIGH);
    digitalWrite(LEFT_MD_B, LOW);

    //Rotate clockwise for right motor
    digitalWrite(RIGHT_MD_A, LOW);
    digitalWrite(RIGHT_MD_B, HIGH);

    //Now turn left and right motors ON!
    analogWrite(LEFT_MOTOR_EN, 100);
    analogWrite(RIGHT_MOTOR_EN, 100);
}
void move_backward()
{

    //Rotate clockwise for left motor
    digitalWrite(LEFT_MD_A, LOW);
    digitalWrite(LEFT_MD_B, HIGH);

    //Rotate counterclockwise for right motor
    digitalWrite(RIGHT_MD_A, HIGH);
    digitalWrite(RIGHT_MD_B, LOW);

    //Now turn left and right motors ON!
    analogWrite(LEFT_MOTOR_EN, 100);
    analogWrite(RIGHT_MOTOR_EN, 100);
}

void turn_left() {
    
    //Rotate clockwise for right motor
    digitalWrite(RIGHT_MD_A, LOW);
    digitalWrite(RIGHT_MD_B, HIGH);

    analogWrite(LEFT_MOTOR_EN, 0);
    analogWrite(RIGHT_MOTOR_EN, 150);
}
void turn_right() {
    
    //Rotate counterclockwise for left motor
    digitalWrite(LEFT_MD_A, HIGH);
    digitalWrite(LEFT_MD_B, LOW);
    
    analogWrite(LEFT_MOTOR_EN, 150);
    analogWrite(RIGHT_MOTOR_EN, 0);
}
void move_stop() {
    
    analogWrite(LEFT_MOTOR_EN, 0);
    analogWrite(RIGHT_MOTOR_EN, 0);
}
void loop() {
    
    if (init_done == false)
    {
        move_forward();
        delay(2000);

        move_stop();
        delay(500);

        turn_right();
        delay(1500);

        move_stop();
        delay(500);

        turn_left();
        delay(1500);

        move_stop();
        delay(500);
       
       move_backward();
        delay(2000);
        init_done = true;
        
    }
    else
    {
        move_stop();
    }
}
```

## SmartCAR UART Example

```cpp

#include "UART_Echo.h"

// Initial serial communication message
unsigned char text[] = "\r\n Welcome! Arduino Mega 2560 \r\n UART0 Test Program.\r\n";

void setup()
{
    int i=0;
    
    // Set the baud rate for serial port.
    Serial.begin(115200);
    
    // Display welcome message.
    // Loop through text array until all letters sent.
    
    while(text[i] !='\0')
        
        Serial.write(text[i++]);
    
    Serial.print("ECHO team Xx >>");
}

 void loop()
{
    // Synopsis : If serial recieves input, write it back to output.
    
    // If Serial.available() is greater than zero, the buffer contains data.
    
    if(Serial.available() > 0) {
        
        // write the data read from serial.
        Serial.write(Serial.read());
    }
}
```

## Setup for Arduino IDE:

1. Go to "Tool"
2. Run the "Serial Monitor" (Ctrl + Shift + M)
3. Go to "Tools"
4. Choose : "Serial Port"
5. Select COMxx
6. Go to "Tools"
7. Select "Serial Monitor"
8. Then set the baud rate to "115200 baud"


## Setup for Bluetooth App:

1. Install "bluetooth spp tools pro" app from Google Play Store.
2. Power on SmartCAR
3. Turn Bluetooth on in Android settings.
4. Scan for Bluetooth device.
5. Click on "155v2.1.7_hb" (or similiar)
6. Check "PIN containing letters or symbols" and Enter "BTWIN", and then Click on "OK"
7. Connection is completed if "Paired" is shown under 155v2.1.7_hb


## Run the Bluetooth Serial App:

1. While SmartCAR is turned ON, and "Bluetooth app Pro" is running on Android device.
2. Click on the already-paired device (155v2.1.7_hb)
3. Click on "Connect" 
4. Click on "Byte stream mode"
5. Connection is completed
6. Push the Reset button on your SmartCAR


