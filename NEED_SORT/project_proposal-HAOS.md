### Haos Project.

## **Project Statement**

* **Goals:**
  * The goal of the project will be to connect the Arduino SmartCar to the Android OS and the sensors it the two devices.

* **Description**
  * **What**
  * The smart car will change its position based on input from the camera in the android phone.
  * **Key Function**
  * Utilization of of image input to control motion.
* **Contribution**
  * The product and methods for production will be posted in public forums for the public's use.
* **Related**
  * There are many attempts and various methods have been explored.
  * Tracking a ball.
  * https://www.youtube.com/watch?v=O6j02lN5gDw
  * Face detection and tracking.
  * http://www.instructables.com/id/Face-detection-and-tracking-with-Arduino-and-OpenC/
  * **Key Difference**
  * Neither projects moves the device/camera in all three dimensions.
* **System overview & architecture**
  * **Block diagram of main blocks**
  * The android OS, and Arduino systems are the two main blocks. Having very little hands on with either of the two, again, time will tell.
  * Further study and understanding is needed for this to be even remotly accurate.
  * **What each block is doing**
  * The Android device will take in images through the camera, analyze them to locate an object and it's positional relation to that object. Once a positional relation is determined, calculations will be made to motivate the smart car to a standard positional relation. 
  * **How each block is connected to other blocks, i.e., interface**
  * Currently it seems the besr way for the Android device to connect would be through the bluetooth interface.
* **Any message is exchanging between blocks? e.g., request/reply, data, etc?**
  * Android Device will need to request forward, backward, and turning actions from the Arduino smart car.
**Project Proposal**
* **Development environment**
  * Arduino Mega 2560-based SmartCAR
  * Android Studio, Androx, Arduino IDE, Internet, etc.
  * **What kind of sensors are to be used in your project**
  * Camera.
  * **Other information from the connection to Android device or other devices? (location, Internet, etc.)**
  * To be determined.
* **Verification procedure**
  * **How can you test if your project works properly as designed**
  * If the device can locate a common ball, hung from a string.
  * If the device can determine the height of the ball above the ground.
  * If the device can motivate itself to a preset distance from the ball's plumb line.
  * If the device can position itself at a specified face of the ball.
  * **Test cases**
  * Get ball.
  * Hang ball from string somewhere in the area of the device.
  * Turn on device.
  * See what happens.
  * (See above for general break-down of each stage.)
* **What do you anticipate will be the easiest part of your project?**
* Finding a ball.
* **What do you anticipate will be the most difficult part of your project?**
* I have little experience with any of this.
* I am finishing my final project for my home school.
* I have a lot of credits.
* **Detailed time plan**

#### **Week 6**	
***2016/04/11(MON)*
* (Due: Project proposal)
* Wait for comments on proposal.

#### **Week 7**	
*2016/04/18(MON)*
* Exam Period.
#### **Week 8**	
*2016/04/25(MON)*
* Determine if object avoidence will be feasable.
#### **Week 9**	
*2016/05/02(MON)*
* General research.
#### **Week 10**	
*2016/05/09(MON)*
* General research.
#### **Week 11**	
*2016/05/16(MON)*
* Connection alrythmically of camera and motors.
#### **Week 12**	
*2016/05/23(MON)*
* Team meeting (for progress check)
#### **Week 13**	
*2016/05/30(MON)*
* Team meeting (for progress check)
#### **Week 14**	
*2016/06/06(MON)*
* Final push for completion.
*(Memorial Day)*
#### **Week 15**	
#### *2016/06/13(MON)*
* Project presentation & demo 
*(Due: source code, presentation slides, & poster slide)*









